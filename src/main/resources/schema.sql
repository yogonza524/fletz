DROP TABLE IF EXISTS fletero;
DROP TABLE IF EXISTS usuario;

CREATE TABLE fletero(
    id varchar(64),
    apellido_y_nombre varchar(200),
    dni varchar(10),
    domicilio varchar(200),
    PRIMARY KEY(id)
);

CREATE TABLE usuario(
    id int(10) PRIMARY KEY AUTO_INCREMENT,
    username varchar(128) NOT NULL,
    password varchar(512) NOT NULL,
    role varchar(128)
);