package com.station.dto;

import lombok.Data;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class FleteroDTO implements Serializable {

    private static final long serialVersionUID = -89393712956881146L;

    @Size(min=10,max=200, message = "El apellido y nombre debe estar entre [10-200] caracteres")
    private String apellidoYNombre;

    @Size(min=5,max=10)
    private String dni;

    @Size(min=10,max=200)
    private String domicilio;
}
