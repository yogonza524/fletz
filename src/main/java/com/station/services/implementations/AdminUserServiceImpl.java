package com.station.services.implementations;

import com.station.services.interfaces.AdminUserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class AdminUserServiceImpl implements AdminUserService {

    @Value("${heroku.username:}")
    private String usernameHeroku;

    @Value("${heroku.password:}")
    private String passwordHeroku;

    @Override
    public boolean isHerokuUser(String username, String password) {
        return usernameHeroku.equals(username) && passwordHeroku.equals(password);
    }
}
