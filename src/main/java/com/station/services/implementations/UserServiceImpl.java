package com.station.services.implementations;

import com.station.model.Usuario;
import com.station.repositories.UsuarioRepository;
import com.station.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public boolean login(String username, String password) {
        if (username != null && password != null && !username.isEmpty() && !password.isEmpty()) {
            List<Usuario> result = usuarioRepository.findByUsernameAndPassword(username,password);
            return result != null && !result.isEmpty();
        }
        return false;
    }
}
