/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.station.controllers;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.station.model.Fletero;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author gonzalo
 */
@CrossOrigin(maxAge = 3600)
@RestController
public class FletzRestController {

    @Autowired
    private List<Fletero> fletersoDummy;

    @Autowired
    private MeterRegistry registry;

    @Autowired
    public JavaMailSender javaMailSender;

    private static final String ACCESS_TOKEN =
            "W5fFi5lAQOUAAAAAAAAaYN6j6Nwl-D-I0ood_mGXuSZxaY2KiJAmicIxndG5F5V1";
    
    private DbxClientV2 client;
    
    @PostConstruct
    public void init() {
        // Create Dropbox client
        DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/fletz").build();
        this.client = new DbxClientV2(config, ACCESS_TOKEN);

        registry.gauge("fleteros_total", Collections.emptyList(), fletersoDummy, List::size);


    }
    
    //JSON -> Javascript Object Notation
    @RequestMapping(value = "/dummyFleteros", method = RequestMethod.GET)
    public Map fletrosDummy() {
        Map<String, Object> result = new HashMap<>();
        
        //key : value
        
        result.put("timestamp", System.currentTimeMillis());
        result.put("status", fletersoDummy.size() > 0 ? 200 : 404);
        result.put("message", fletersoDummy.size() > 0 ? "Se encontraron datos" : "No hay fleteros en la lista");
        result.put("data", fletersoDummy);
        
        return result;
    }
    
    @RequestMapping(value = "/dummyFleteros/{nombreFletero}", method = RequestMethod.GET)
    public Map fleterosPorNombre(@PathVariable("nombreFletero") String nombre) {
        Map<String, Object> result = new HashMap<>();
        
        Fletero buscado = null;
        
        for (Fletero f : fletersoDummy) {
            if (f.getApellidoYNombre().equalsIgnoreCase(nombre)) {
                buscado = f;
                break;
            }
        }
        
        result.put("timestamp", System.currentTimeMillis());
        result.put("data", buscado != null ? buscado : "No lo encontre");
        result.put("status", buscado != null ? 200 : 404);
        
        return result;
    }
    
    @RequestMapping(value = "/emojis", method = RequestMethod.GET)
    public Map emojis() {
        Map<String, Object> result = new HashMap<>();
        
        return result;
    }
    
    @RequestMapping(value = "/uploadImage/{id}", method=RequestMethod.POST)
    public Map subirImagen(@RequestParam("image") MultipartFile[] files,
            @PathVariable("id") String id) throws DbxException, IOException {
        Map<String,Object> result = new HashMap<>();
        
        if (files != null && files[0] != null) {
            //Image OK, let's store at Dropbox
            File imageFile = convert(files[0]);
            // Upload "file" to Dropbox
            try (InputStream in = new FileInputStream(imageFile)) {
                client.files()
                        .uploadBuilder("/" + files[0].getOriginalFilename())
                    .uploadAndFinish(in);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(FletzRestController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(FletzRestController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            result.put("status",200);
            result.put("imagen subida",files[0].getOriginalFilename());
        }
        else {
            System.out.println("Error");
            result.put("Error","No hay imagenes");
        }
        
        return result;
    }
    
    public File convert(MultipartFile file) throws IOException
    {    
        File convFile = new File(file.getOriginalFilename());
        convFile.createNewFile(); 
        FileOutputStream fos = new FileOutputStream(convFile); 
        fos.write(file.getBytes());
        fos.close(); 
        return convFile;
    }

    public void sendHTMLMessage(String to, String subject, String text) throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());

        helper.setTo(to);
        helper.setText(text, true);
        helper.setSubject(subject);
        helper.setFrom("fletz.gonza@gmail.com");

        javaMailSender.send(message);
    }

    @RequestMapping(value= "/r/p/{latitude}/{longitude}", method = RequestMethod.POST)
    public String pos(@PathVariable("latitude") String latitude, @PathVariable("longitude") String longitude) {
        try {
            sendHTMLMessage("yogonza524@gmail.com", "GEO", "Latitud: " + latitude + ". Longitude: " + longitude);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return "";
    }
}
