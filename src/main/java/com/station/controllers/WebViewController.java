/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.station.controllers;

import com.station.dto.FleteroDTO;
import com.station.model.Fletero;

import java.nio.charset.StandardCharsets;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;

/**
 *
 * @author gonzalo
 */
@Controller
public class WebViewController {

    @Autowired
    private List<Fletero> fletersoDummy;

    @RequestMapping(value = "/fleteros", method = RequestMethod.GET)
    public String fleteros(Model model) {

        model.addAttribute("fleteros", fletersoDummy);

        return "fleteros";
    }

    @RequestMapping(value = "/contacto", method = RequestMethod.GET)
    public String contacto(Model model) {

        return "contacto";
    }

    @RequestMapping(value = "/registrarme", method = RequestMethod.GET)
    public String registrarme(Model model, FleteroDTO fleteroDTO) {

        return "registrarme";
    }

    @RequestMapping(value = "/registrarme", method = RequestMethod.POST)
    public String checkFleteroInfo(@Valid FleteroDTO fleteroDTO, BindingResult bindingResult) {

//        Registrarlo en Base de datos o hacer algo
//        this.fleteroService.save(fleteroDTO);

        if (bindingResult.hasErrors()) {

            return "registrarme";
        }
        //Hasta aca llego solo si fleteroDTO pasó las validaciones
//        model.addAttribute("fleteroNuevo", fleteroDTO);

        return "redirect:/altaFleteroOK";
    }

    @RequestMapping(value = "/altaFleteroOK", method = RequestMethod.GET)
    public String altaDeFleteroOk(Model model) {
        return "altaFleteroOK";
    }

    @RequestMapping(value = "/acceder", method = RequestMethod.GET)
    public String acceder(Model model) {
        
        return "acceder";
    }
    
    @RequestMapping(value = "/header", method = RequestMethod.GET)
    public String header(Model model) {
        
        return "components/acceder";
    }
    
    @RequestMapping(value = "/ficha", method = RequestMethod.GET)
    public String ficha(Model model, @RequestParam("id") String idFletero) {
        
        //1. Tengo el id del fletero y una lista de fleteros
        for (Fletero fletero : this.fletersoDummy) {
            //Verifico
            if (fletero.getId().equals(idFletero)) {
                model.addAttribute("fletero", fletero);
                break;
            }
        }
        
        return "ficha";
    }
    
    @RequestMapping(value = "/subirImagen", method = RequestMethod.GET)
    public String subirImagen(Model model) {
        
        return "subirImagen";
    }

    @RequestMapping(value = "/")
    public String index(Model model) {
        return "index";
    }
}
