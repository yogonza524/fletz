package com.station.repositories;

import com.station.model.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario,Long> {

    @Query(value = "SELECT u FROM Usuario u WHERE u.username = :username AND u.password = :password")
    List<Usuario> findByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    @Query(value = "SELECT u FROM Usuario u WHERE u.username = :username")
    List<Usuario> findByUsername(@Param("username") String username);
}
