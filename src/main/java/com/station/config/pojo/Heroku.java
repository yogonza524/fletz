package com.station.config.pojo;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class Heroku implements Serializable {

    private static final long serialVersionUID = 423975063850566846L;

    private Release release;
}
