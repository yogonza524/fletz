package com.station.config.security;

import com.station.services.interfaces.AdminUserService;
import com.station.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class SecurityAuth implements AuthenticationProvider {

    @Autowired
    private UserService userService;

    @Autowired
    private AdminUserService adminUserService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        if (userService.login(username, password)) {

            // use the credentials
            // and authenticate against the database
            return new UsernamePasswordAuthenticationToken(
                    username, password, new ArrayList<>());
        }
        if (adminUserService.isHerokuUser(username,password)) {

            return new UsernamePasswordAuthenticationToken(
                    username, password, new ArrayList<>());
        }

        return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
