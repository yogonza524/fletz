package com.station.elasticRepositories;

import com.station.elasticModel.FleteroContrato;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface FleteroContratoRepository extends ElasticsearchRepository<FleteroContrato, String> {

    Page<FleteroContrato> findByIdFletero(String idFletero, Pageable page);
}
