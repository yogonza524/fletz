package com.station.fletz;

import com.station.elasticModel.FleteroContrato;
import com.station.services.interfaces.FleteroContratoService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Description;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.matchPhraseQuery;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class ElasticTest {

    @Autowired
    private FleteroContratoService fleteroContratoService;

    @Autowired
    private ElasticsearchOperations elasticsearchTemplate;

    private FleteroContrato fleteroContrato;

    @Before
    public void init() {
        this.fleteroContrato = FleteroContrato.builder()
                .idFletero("123")
                .largaDistancia(true)
                .precioKilometro(235.4)
                .build();
        //Delete all documents for consistent test
        dropIndex(FleteroContrato.class);
    }

    @Test
    public void insertTest() {
        FleteroContrato result = fleteroContratoService.save(this.fleteroContrato);
        assertNotNull(result);
    }

    @Test
    @Description("Siempre que se limpie el indice 'contrato' en el método init() no deben existir documentos en ese indice")
    public void findOneByIdFleteroTest() {
        String idFletero = "123";
        Page<FleteroContrato> result = fleteroContratoService.findByIdFletero(idFletero, new PageRequest(0,1));
        assertNotNull(result);
        assertEquals(0,result.getTotalElements());
    }

    @Test
    public void listAllTest() {
        FleteroContrato result = fleteroContratoService.save(
                FleteroContrato.builder().idFletero("456")
                .precioKilometro(126.7)
                .largaDistancia(false)
                .build()
        );
        assertNotNull(result);

        fleteroContratoService.findAll().forEach(System.out::println);
        assertEquals(1L, fleteroContratoService.findAll().size());
    }

    @Test
    public void shouldExecuteGivenCriteriaQueryAndGetOneFleteroContrato() {
        // given
        FleteroContrato fc = fleteroContratoService.save(
                FleteroContrato.builder().idFletero("456")
                        .precioKilometro(126.7)
                        .largaDistancia(false)
                        .clausulas("El cliente debe proveer del ayudante. Solo se provee el transporte")
                        .build()
        );

        CriteriaQuery criteriaQuery = new CriteriaQuery(new Criteria("clausulas").contains("ayudante"));

        // when
        FleteroContrato result = elasticsearchTemplate.queryForObject(criteriaQuery, FleteroContrato.class);
        // then
        assertThat(result, is(notNullValue()));
    }

    @Test
    public void shouldExecuteGivenCriteriaQueryAndGetListFleteroContrato() {
        // given
        //a contract
        fleteroContratoService.save(
                FleteroContrato.builder().idFletero("456")
                        .precioKilometro(126.7)
                        .largaDistancia(false)
                        .clausulas("El cliente debe proveer del ayudante. Solo se provee el transporte")
                        .build()
        );
        //and another
        fleteroContratoService.save(
                FleteroContrato.builder().idFletero("225")
                        .precioKilometro(145.1)
                        .largaDistancia(true)
                        .clausulas("El ayudante solo trabaja por la mañana. El transportista hace de ayudante")
                        .build()
        );

        CriteriaQuery criteriaQuery = new CriteriaQuery(new Criteria("clausulas").contains("ayudante"));

        // when
        List<FleteroContrato> result = elasticsearchTemplate.queryForList(criteriaQuery, FleteroContrato.class);
        // then
        assertThat(result, is(notNullValue()));
        assertThat(result.size(), is(2));
    }

    @Test
    public void deleteOneEntityTest() {
        //given fleteroContrato loaded at init()

        //when
        //save at Elastic Search Server
        FleteroContrato result = this.fleteroContratoService.save(fleteroContrato);
        assertNotNull(result);

        //then
        this.fleteroContratoService.delete(result);
        assertThat(null, is(this.fleteroContratoService.findOne(result.getId())));

    }

    //for init the index, used by @Before
    private void dropIndex(Class<?> index) {
        elasticsearchTemplate.deleteIndex(index);
        elasticsearchTemplate.createIndex(index);
        elasticsearchTemplate.putMapping(index);
        elasticsearchTemplate.refresh(index);
    }
}
